<?php

include_once 'models/usuario.php';
include_once 'controller-base.php';

class CriarContaController extends ControllerBase {
    function get() {

    }

    function post() {
        $model = new Usuario();

        $usuario = $this->extrair_post(
            'nome',
            'email',
            'senha',
            'telefone',
            'cidade',
            'endereco',
            'estado'
        );

        try {
            $model->criar($usuario);
            $this->redirect = 'login';
        } catch (ModelException $e) {
            $this->bag->add('erro', $e->getMessage());
        } catch (PDOException $e) {
            if (strpos($e->getMessage(), $usuario['email']) === false) {
                throw $e;
            }
            $this->bag->add('erro', 'E-mail em uso por outro usuário');
        }
    }
}

(new CriarContaController())->tratar_req();
