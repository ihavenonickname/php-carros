<?php

class BancoDeDados {
    private static $instancia = null;

    public static function get() {
        if (BancoDeDados::$instancia === null) {
            BancoDeDados::$instancia = new BancoDeDados();
        }

        return BancoDeDados::$instancia;
    }

    var $pdo;

    public function __construct() {
        $host = '127.0.0.1';
        $db = 'php_veiculos';
        $user = 'root';
        $pass = '';
        $charset = 'utf8mb4';

        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false
        ];
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

        $this->pdo = new PDO($dsn, $user, $pass, $opt);
    }

    function selecionar($query, $params = null) {
        $comando = $this->pdo->prepare($query);
        $comando->execute($params);

        while ($linha = $comando->fetch()) {
            yield $linha;
        }
    }

    function executar($query, $params = null) {
        $comando = $this->pdo->prepare($query);
        return $comando->execute($params);
    }
}
