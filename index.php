<?php

include_once 'models/usuario.php';
include_once 'controller-base.php';

class IndexController extends ControllerBase {
    function __construct() {
        $this->metodos_auth = ['get'];
    }

    function get() {
        $model = new Usuario();
        $usuario = $model->buscar($this->id_usuario);

        $this->bag->add('usuario', $usuario);
    }
}

(new IndexController())->tratar_req();
