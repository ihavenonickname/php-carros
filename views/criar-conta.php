<!DOCTYPE html>

<meta charset="UTF-8">

<link rel="stylesheet" href="static/master.css">

<title>PHP Veículos</title>

<h1>Criar nova conta</h1>

<div class="container">
    <div class="content">
        <form class="formulario" method="post">
            <div class="formulario-input">
                <input type="text" placeholder=" " name="nome">
                <label>Nome</label>
            </div>

            <div class="formulario-input">
                <input type="text" placeholder=" " name="email">
                <label>E-mail</label>
            </div>

            <div class="formulario-input">
                <input type="password" placeholder=" " name="senha">
                <label>Senha</label>
            </div>

            <div class="formulario-input">
                <input type="text" placeholder=" " name="telefone">
                <label>Telefone</label>
            </div>

            <div class="formulario-input">
                <input type="text" placeholder=" " name="cidade">
                <label>Cidade</label>
            </div>

            <div class="formulario-input">
                <input type="text" placeholder=" " name="endereco">
                <label>Endereço</label>
            </div>

            <div class="formulario-select">
                <select name="estado">
                    <option value="AC">AC</option>
                    <option value="AL">AL</option>
                    <option value="AP">AP</option>
                    <option value="AM">AM</option>
                    <option value="BA">BA</option>
                    <option value="CE">CE</option>
                    <option value="DF">DF</option>
                    <option value="ES">ES</option>
                    <option value="GO">GO</option>
                    <option value="MA">MA</option>
                    <option value="MT">MT</option>
                    <option value="MS">MS</option>
                    <option value="MG">MG</option>
                    <option value="PA">PA</option>
                    <option value="PB">PB</option>
                    <option value="PR">PR</option>
                    <option value="PE">PE</option>
                    <option value="PI">PI</option>
                    <option value="RJ">RJ</option>
                    <option value="RN">RN</option>
                    <option value="RS">RS</option>
                    <option value="RO">RO</option>
                    <option value="RR">RR</option>
                    <option value="SC">SC</option>
                    <option value="SP">SP</option>
                    <option value="SE">SE</option>
                    <option value="TO">TO</option>
                </select>

                <label>Estado</label>
            </div>

            <input type="submit" value="Criar" class="botao">
        </form>

        <?php if ($bag->tem('erro')) { ?>
            <span class="erro"><?php echo $bag->erro ?></span>
        <?php } ?>
    </div>
</div>
