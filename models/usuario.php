<?php

include_once 'utils/banco.php';
include_once 'model-exception.php';

class Usuario {
    function fazer_login($email, $senha) {
        $query = '
            select id, senha
            from usuarios
            where email = :email
            limit 1';

        $cursor = BancoDeDados::get()->selecionar($query, ['email' => $email]);

        $id = null;

        foreach ($cursor as $linha) {
            if (password_verify($senha, $linha['senha'])) {
                $id = $linha['id'];
            }
        }

        if ($id === null) {
            return null;
        }

        $bad_uuid = uniqid();

        $query = '
            update usuarios
            set token_login = :token_login, momento_geracao_token = now();';

        BancoDeDados::get()->executar($query, ['token_login' => $bad_uuid]);

        return $bad_uuid;
    }

    function verificar_tamanho_str($obj, $prop, $min, $max) {
        $len = strlen($obj[$prop]);

        if ($len < $min || $len > $max) {
            $x = ucfirst($prop);
            throw new ModelException("O campo '$x' deve ter entre $min e $max caracteres");
        }
    }

    function criar($usuario) {
        $this->verificar_tamanho_str($usuario, 'nome', 1, 50);
        $this->verificar_tamanho_str($usuario, 'email', 1, 100);
        $this->verificar_tamanho_str($usuario, 'senha', 1, 15);
        $this->verificar_tamanho_str($usuario, 'telefone', 1, 20);
        $this->verificar_tamanho_str($usuario, 'endereco', 1, 100);
        $this->verificar_tamanho_str($usuario, 'cidade', 1, 100);

        if (!filter_var($usuario['email'], FILTER_VALIDATE_EMAIL)) {
            throw new ModelException('E-mail inválido');
        }

        $siglas = [
            'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO',
            'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI',
            'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'
        ];

        if (!in_array($usuario['estado'], $siglas)) {
            throw new ModelException('Estado inválido');
        }

        $query = '
            insert into usuarios
            (nome, email, senha, telefone, endereco, cidade, estado)
            values
            (:nome, :email, :senha, :telefone, :endereco, :cidade, :estado)';

        $usuario['senha'] = password_hash($usuario['senha'], PASSWORD_DEFAULT);

        BancoDeDados::get()->executar($query, $usuario);
    }

    function buscar($id) {
        $query = '
            select *
            from usuarios
            where id = :id
            limit 1';

        $cursor = BancoDeDados::get()->selecionar($query, ['id' => $id]);

        foreach ($cursor as $linha) {
            return $linha;
        }

        return null;
    }

    function verificar_validade_token_login($token) {
        $query = '
            select id
            from usuarios
            where token_login = :token_login and
            timestampdiff(DAY, momento_geracao_token, CURRENT_TIMESTAMP()) < 1
            limit 1';

        $cursor = BancoDeDados::get()->selecionar($query, ['token_login' => $token]);

        foreach ($cursor as $linha) {
            return $linha['id'];
        }

        return null;
    }
}
