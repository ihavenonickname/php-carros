<?php

class ViewBag {
    var $dados;

    function __construct() {
        $this->dados = [];
    }

    function add($nome, $valor) {
        $this->dados[$nome] = $valor;
    }

    function add_flat($array) {
        foreach ($array as $chave => $valor) {
            $this->add($chave, $valor);
        }
    }

    function tem($nome) {
        return array_key_exists($nome, $this->dados);
    }

    function pegar($nome) {
        return  $this->dados[$nome];
    }

    function __get($nome) {
        return $this->pegar($nome);
    }
}
