create database if not exists php_veiculos;

use php_veiculos;

create table if not exists usuarios (
    id int auto_increment primary key,
    nome varchar(50) not null,
    email varchar(100) not null unique,
    senha varchar(255) not null, -- https://secure.php.net/manual/en/function.password-hash.php
    telefone varchar(20) not null,
    endereco varchar(100) not null,
    cidade varchar(100) not null,
    estado varchar(2) not null,
    token_login varchar(36) null,
    momento_geracao_token timestamp null
);

create table if not exists veiculos (
    id int auto_increment primary key,
    usuario_id int not null references usuarios (id),
    marca varchar(50) not null,
    modelo varchar(50) not null,
    ano year not null,
    quilometragem int not null,
    valor float not null,
    descricao varchar(300)
);

create table if not exists fotos_veiculos (
    veiculo_id int not null references veiculos (id),
    href text not null,
    principal tinyint not null default 0
);
