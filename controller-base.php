<?php

include_once 'models/usuario.php';
include_once 'utils/view-bag.php';

abstract class ControllerBase {
    var $bag = null;
    var $redirect = null;
    var $id_usuario = null;
    var $metodos_auth = [];

    function autenticar() {
        if (isset($_COOKIE['auth'])) {
            $model = new Usuario();
            $this->id_usuario = $model->verificar_validade_token_login($_COOKIE['auth']);
        }
    }

    function tratar_req() {
        $metodo = strtolower($_SERVER['REQUEST_METHOD']);
        $metodos_permitidos = ['post', 'get', 'put', 'delete'];

        if (!in_array($metodo, $metodos_permitidos) || !method_exists($this, $metodo)) {
            http_response_code(405);
            return;
        }

        $this->bag = new ViewBag();

        if (in_array($metodo, $this->metodos_auth)) {
            $this->autenticar();

            if ($this->id_usuario !== null) {
                $this->$metodo();
            } else {
                http_response_code(401);
                $this->redirect = 'login';
            }
        } else {
            $this->$metodo();
        }

        if ($this->redirect === null) {
            $bag = $this->bag;
            include_once 'views/' . $_SERVER['PHP_SELF'];
        } else {
            header('Location: /' . $this->redirect . '.php');
        }
    }

    function extrair_post() {
        $obj = [];

        foreach (func_get_args() as $chave) {
            if (isset($_POST[$chave])) {
                $obj[$chave] = $_POST[$chave];
            }
        }

        return $obj;
    }
}
