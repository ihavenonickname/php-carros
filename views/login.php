<!DOCTYPE html>

<meta charset="UTF-8">

<link rel="stylesheet" href="static/master.css">

<style>
a {
    margin-top: 50px;
}
</style>

<title>PHP Veículos</title>

<h1>PHP Carros</h1>

<div class="container">
    <div class="content">
        <form class="formulario" method="post">
            <div class="formulario-input">
                <input type="text" placeholder=" " name="email">
                <label>E-mail</label>
            </div>

            <div class="formulario-input">
                <input type="password" placeholder=" " name="senha">
                <label>Senha</label>
            </div>

            <input type="submit" value="Entrar" class="botao">
        </form>

        <?php if ($bag->tem('erro')) { ?>
            <span class="erro"><?php echo $bag->erro ?></span>
        <?php } ?>

        <a href="criar-conta.php">Quero criar uma conta</a>
    </div>
</div>
