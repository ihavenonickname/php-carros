<?php

include_once 'models/usuario.php';
include_once 'controller-base.php';

class LoginController extends ControllerBase {
    function get() {

    }

    function post() {
        $model = new Usuario();
        $token = $model->fazer_login($_POST['email'], $_POST['senha']);

        if ($token !== null) {
            setcookie('auth', $token, 0, '/');
            $this->redirect = 'index';
        } else {
            $this->bag->add('erro', 'Usuário inválido ou senha inválida');
        }
    }
}

(new LoginController())->tratar_req();
